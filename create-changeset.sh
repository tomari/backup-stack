#!/bin/sh
aws cloudformation --region us-west-2 create-change-set --stack-name backup-stack --template-body file://backup-stack.yaml --capabilities CAPABILITY_NAMED_IAM --change-set-name "$1"
